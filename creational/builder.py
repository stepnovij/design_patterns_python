"""
The Builder design pattern is a creational design pattern, designed to provide a
flexible design solution to various object creation problems in Object-Oriented software.
The intent of the Builder design pattern is to separate the construction of a complex object from its representation.
It is one of the GoF design patterns.

The Builder design pattern solves problems like:

How can a class (the same construction process) create different representations of a complex object?
How can a class that includes creating a complex object be simplified? [1]

1. wikipedia https://en.wikipedia.org/wiki/Builder_pattern
"""


class Table:
    def __init__(self, num_rows=None, num_cols=None, labels=None, pictures=None, buttons=None):
        self.num_rows = num_rows
        self.num_cols = num_cols
        self.labels = labels
        self.pictures = pictures
        self.buttons = buttons

    def draw_table(self):
        # some action of drawing table
        return


class TableBuilder:
    def __init__(self):
        self.table = Table()

    def set_rows(self, rows):
        self.table.num_rows = rows

    def set_columns(self, columns):
        self.table.num_cols = columns

    def set_labels(self, labels):
        self.table.labels = labels

    def set_pictures(self, pictures):
        self.table.pictures = pictures

    def get_result(self):
        return self.table.draw_table()


if __name__ == '__main__':
    builder = TableBuilder()
    builder.set_columns(10)
    builder.set_rows(10)
    builder.set_labels(['1', '2', '3', '4'])
    builder.get_result()
