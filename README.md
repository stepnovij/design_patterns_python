# Design patterns in Python

## implemented:

* creational:

    * singleton
    * abstract factory
    * factory method
    * prototype
    * builder

* structural:

    * bridge
    * adapter
    * proxy
    * facade
    * decorator
    * composite
    * flyweight
  
* behavioral

    * chain of command
    * iterator
    * command
    * mediator
    
### sources of takeaways and keynotes:

* https://refactoring.guru/
* https://sourcemaking.com/
* https://en.wikipedia.org/wiki/Software_design_pattern 
