"""
decoupling sender and reciever of the request
let handle more by one recievers request

like many other behaviour patterns ChoR relies on transforming behaviours into object
"""
import logging

LOGGING_FORMAT = '%(asctime)s - %(message)s'
logging.basicConfig(level=logging.INFO, format=LOGGING_FORMAT)


class BaseHandler:
    def __init__(self):
        self._handlers = []
        self._successor = None

    def handle(self, req):
        self._successor._handle(req)

    def set_next(self, handler):
        self._successor = handler

    def _handle(self, req):
        pass


class RequestHandler(BaseHandler):
    def __init__(self):
        super().__init__()
        self.logger = logging

    def _handling_request(self, req):
        logging.info("start working with request, my id is %s", id(self))
        # here should be anything interesting with logging
        logging.info("finished working with request, my id is %s", id(self))

    def _handle(self, req):
        if req and req.data:
            logging.info("handling request")
            self._handling_request(req)
        if self._successor is not None:
            logging.info("moving request to the next handler")
            self.handle(req)
        else:
            logging.info("no data, no successor request")
            return


class Request:
    def __init__(self, data=None):
        self.data = data


if __name__ == '__main__':
    rh = RequestHandler()
    rh1 = RequestHandler()
    rh2 = RequestHandler()
    rh3 = RequestHandler()
    #
    rh.set_next(rh1)
    rh1.set_next(rh2)
    rh2.set_next(rh3)
    #
    req = Request()
    rh.handle(req)
    req2 = Request('some test data')
    rh.handle(req2)