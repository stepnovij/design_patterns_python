"""
Composite is a structural design pattern that lets you compose objects into
tree structures and allow clients to work with these structures as if they were individual objects.

TakeAways:
1. When you have to implement a tree-like structure that has simple elements and containers.
2. When Clients should treat simple and complex elements uniformly.
"""
import random


class Thing:
    def get_price(self):
        pass


class Product(Thing):
    def get_price(self):
        return random.randint(1, 10)


class Box(Thing):
    def __init__(self):
        self.things = []

    def add(self, thing):
        self.things.append(thing)

    def get_price(self):
        total_price = 0
        for th in self.things:
            total_price += th.get_price()
        return total_price


class Client:
    def __init__(self, things):
        self.things = things

    def get_price_from_things(self):
        return self.things.get_price()


if __name__ == '__main__':
    #
    p1 = Product()
    p2 = Product()
    p3 = Product()
    p4 = Product()
    p5 = Product()
    #
    b1 = Box()
    b2 = Box()
    b3 = Box()
    #
    b1.add(p1)
    b1.add(p2)
    b1.add(p3)
    #
    b2.add(p4)
    b2.add(p5)
    b2.add(b1)
    #
    c = Client(b2)
    print(c.get_price_from_things())