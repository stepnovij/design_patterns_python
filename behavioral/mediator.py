"""
Motivation: encapsulate set of objects to make them independent

Great analogy: control tower

concrete mediator is tightly coupled to the components. extract depndecies from set of the objects in
separate class
"""
import logging


LOGGING_FORMAT = '%(asctime)s - %(message)s'
logging.basicConfig(level=logging.INFO, format=LOGGING_FORMAT)


class BaseMediator:

    def notify(self, sender, msg):
        raise NotImplemented


class ConcreteMediator(BaseMediator):

    def notify(self, sender, msg):
        if sender == "ComponentA":
            logging.info("We've go smth interesting from ComponentA: %s", msg)
        elif sender == "ComponentB":
            logging.info("We've go smth interesting from ComponentB: %s", msg)
        else:
            logging.info("We've go smth interesting from ComponentC: %s", msg)


class BaseComponent:
    def __init__(self, mediator):
        self.mediator =mediator


class ComponentA(BaseComponent):
    def operationA(self):
        logging.info("Making something known as operationA")
        self.mediator.notify(self.__class__.__name__, 'msg from operation A')
        return


class ComponentB(BaseComponent):
    def operationB(self):
        logging.info("Making something known as operationB")
        self.mediator.notify(self.__class__.__name__, 'msg from operation B')
        return


class ComponentC(BaseComponent):
    def operationC(self):
        logging.info("Making something known as operationC")
        self.mediator.notify(self.__class__.__name__, 'msg from operation C')
        return


if __name__ == "__main__":
    m = ConcreteMediator()
    #
    a = ComponentA(m)
    b = ComponentB(m)
    c = ComponentC(m)
    #
    a.operationA()
    b.operationB()
    c.operationC()