"""
Abstract Factory is a creational design pattern that lets you produce FAMILIES of related objects
without specifying their concrete classes.

TakeAway:
1. Often, designs start out using Factory Method (less complicated, more customizable via subclasses)
and evolve toward Abstract Factory, Prototype, or Builder (more flexible, but more complex)
as the designer discovers where more flexibility is needed.
2. Builder focuses on constructing a complex object step by step.
Abstract Factory creates families of product objects (either simple or complex).
Builder returns the product as a final step, but the Abstract Factory returns the result immediately.
"""


class AbstractFactoryClient:
    def connect(self):
        pass


class ClientMysqlFactory(AbstractFactoryClient):
    def __init__(self, connection_string):
        self.connection_string = connection_string

    def connect(self):
        # some logic
        return


class ClientPostgreSQLFactory(AbstractFactoryClient):
    def __init__(self, connection_string):
        self.connection_string = connection_string

    def connect(self):
        # some logic
        return


class Query:
    def __init__(self, client):
        self.client = client

    def get_data(self):
        pass


class MysqlQuery(Query):
    def get_data(self):
        return 'MYSQL DATA'


class PostgreSQLQuery(Query):
    def get_data(self):
        return 'POSTGRESQL DATA'


class AbstractFactoryDbConnection:
    def connect(self):
        print(self.__class__.__name__)
        return


class DbConnectionPostgreSQL(AbstractFactoryDbConnection):
    def __init__(self, connection_string):
        self.connection_string = connection_string

    def connect(self):
        super().connect()
        return ClientPostgreSQLFactory(self.connection_string)


class DbConnectionMysql(AbstractFactoryDbConnection):
    def __init__(self, connection_string):
        self.connection_string = connection_string

    def connect(self):
        super().connect()
        return ClientMysqlFactory(self.connection_string)


def get_data(connection_string):
    if 'mysql' in connection_string:
        factory = DbConnectionMysql(connection_string)
        factory.connect()
        data = MysqlQuery(factory).get_data()
    else:
        factory = DbConnectionPostgreSQL(connection_string)
        factory.connect()
        data = PostgreSQLQuery(factory).get_data()
    return data


if __name__ == '__main__':
    #
    connection_string = 'postgres://'
    data = get_data(connection_string)
    #
    connection_string = 'mysql://'
    data = get_data(connection_string)
