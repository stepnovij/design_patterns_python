class Singleton:
    """
    Simple Singleton
    """
    _instance = None

    def __new__(cls):
        if not cls._instance:
            instance = super().__new__(cls)
            cls._instance = instance
            return instance
        else:
            print('Object was already created')
            return cls._instance


def singleton_function(cls):
    instances = {}

    def wrapper(*args, **kwargs):
        if cls not in instances:
            instances[cls] = cls(*args, **kwargs)
        else:
            print('Object was already created')

        return instances[cls]
    return wrapper


@singleton_function
class A:
    pass


if __name__ == '__main__':
    s = Singleton()
    print(id(s))
    #
    k = Singleton()
    print(id(k))
    #
    a = A()
    print(id(a))
    #
    b = A()
    print(id(b))