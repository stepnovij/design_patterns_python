"""
The Proxy pattern suggests creating a substitute class that has the same interface as an original
service object. Upon receiving the request from a client, the proxy object creates an instance
of a service object and delegate it all real work.


TakeAways:
1. Decorator and Proxy have similar structures but different purposes.
Both patterns built on the composition principle of delegating work to other object.
However, the Proxy manages the life cycle of its service object by itself,
whereas Decorator structure is controlled by client.
"""

import logging
LOGGING_FORMAT = '%(asctime)s - %(message)s'
logging.basicConfig(level=logging.INFO, format=LOGGING_FORMAT)


def do_smth1():
    return


def do_smth2():
    return


class A:
    def get_data(self):
        logging.info(self.__class__.__name__)
        do_smth1()
        return


class ProxyA:
    def __init__(self, a):
        self._a = a

    def get_data(self):
        logging.info(self.__class__.__name__)
        self._a.get_data()
        do_smth2()
        return


if __name__ == '__main__':
    pass