"""
TakeAways:

1. Designs that make heavy use of the Composite and Decorator patterns often can benefit
from Prototype as well. It would allow to clone complex structures,
instead of re-constructing them from scratch.
2. Abstract Factory, Builder and Prototype can be implemented as Singletons.
3. Often, designs start out using Factory Method (less complicated, more customizable via subclasses)
and evolve toward Abstract Factory, Prototype, or Builder (more flexible, but more complex)
 as the designer discovers where more flexibility is needed.
"""
import logging


LOGGING_FORMAT = '%(asctime)s - %(message)s'
logging.basicConfig(level=logging.INFO, format=LOGGING_FORMAT)


class Prototype:
    def clone(self):
        raise NotImplemented


class Cell:
    logger = logging

    def __init__(self, size, type, age):
        self.logger.info('Creating new cell')
        self._size = size
        self._type = type
        self._age = age


    @property
    def size(self):
        return self._size

    @property
    def age(self):
        return self._age

    @property
    def type(self):
        return self._type

    def clone(self):
        self.logger.info('Calling clone method')
        return Cell(self._size, self._type, self._age)


if __name__ == '__main__':
    cell1 = Cell(10, 'muscle', 0.1)
    cell2 = cell1.clone()
    assert id(cell1) != id(cell2)
