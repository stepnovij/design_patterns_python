"""
The Factory Method pattern suggests replacing direct object creation (using a new operator)
with a call to a special "factory" method.
The constructor call should be moved inside that method.
Objects returned by factory methods are often referred to as "products."

A factory method does not have to create new instances all the time.
It can also return existing objects from some cache, etc.

TakeAways:
1. When you do not know the exact types and dependencies of the objects, your code should work with
2. Factory method can hide the implementation details of a product from the other code.
3. Follows the Open/Closed Principle.
4. Often, designs start out using Factory Method (less complicated, more customizable via subclasses)
and evolve toward Abstract Factory, Prototype, or Builder (more flexible, but more complex)
as the designer discovers where more flexibility is needed.
5. Prototype doesn't require subclassing, but it does require an "initialize" operation.
Factory Method requires subclassing, but doesn't require initialization step.
"""

import logging

LOGGING_FORMAT = '%(asctime)s - %(message)s'
logging.basicConfig(level=logging.INFO, format=LOGGING_FORMAT)


class Transport:
    def send(self):
        raise NotImplemented


class TCPTransport(Transport):
    def send(self):
        logging.info('Sending TCP data')


class UDPTransport(Transport):
    def send(self):
        logging.info('Sending UDP data')


class Connection:
    def connect_and_send(self, type_):
        self.connect()
        self.send(type_)

    def connect(self):
        logging.info('connecting')

    def send(self, type_):
        raise NotImplemented


class TCPConnection(Connection):
    def send(self, type_):
        TCPTransport().send()


class UDPConnection(Connection):
    def send(self, type_):
        UDPTransport().send()


class App:
    def send(self, type_):
        logging.info('starting APP')
        if type_ == 'UDP':
            UDPConnection().connect_and_send(type_)
        else:
            TCPConnection().connect_and_send(type_)


if __name__ == '__main__':
    App().send('UDP')
    App().send('TCP')
