"""
Flyweight is a structural design pattern that lets you fit more objects into the available amount
of RAM by sharing common parts of object state among multiple objects,
instead of keeping it in each object.

extrinsic state - changeable from outside of the context
intrinsic state - unchangeable data

Actually this pattern divides objects into two parts: flyweights and context

Flyweight use same intrisic state that can be shared through other objects,
extrinsic state passes as context - more or less unique

From client point of view flyweight is a template that can be configured at runtime
"""


class PointType:
    def __init__(self, type, colour, texture):
        self._type = type
        self._colour = colour
        self._texture = texture

    @property
    def type(self):
        return self._type

    @property
    def colour(self):
        return self._colour

    @property
    def texture(self):
        return self._texture


class PointTypeFactory:
    _instance = None
    _pool = []

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
            return cls._instance
        return cls._instance

    def get_point(self, type, colour, texture):
        for item in self._pool:
            if item.type == type and item.colour == colour and item.texture == texture:
                return item
        point = PointType(type, colour, texture)
        self._pool.append(point)
        return point


class Point:
    def __init__(self, x, y, _type, colour, texture):
        self._x = x
        self._y = y
        self._type = PointTypeFactory().get_point(_type, colour, texture)

    def draw(self):
        print(
            'Drawing {} point on {} {} for type_id {} colour {} texture {}'.format(
                id(self), self._x, self._y, id(self._type), self._type.colour, self._type.texture
            )
        )


class Image:
    def __init__(self, points=None):
        if points is None:
            self.points = []
        else:
            self.points = points

    def add_point(self, x, y, _type, colour, texture):
        self.points.append(Point(x, y, _type, colour, texture))

    def draw(self):
        for point in self.points:
            point.draw()


if __name__ == "__main__":
    image = Image()
    image.add_point(20, 30, 'light', 'yellow', 'smooth')
    image.add_point(10, 10, 'light', 'yellow', 'smooth')
    image.add_point(30, 20, 'light', 'red', 'smooth')
    image.add_point(40, 12, 'light', 'red', 'smooth')
    image.add_point(50, 5, 'light', 'red', 'smooth')
    image.add_point(21, 30, 'dark-light', 'orange', 'not-smooth')
    image.add_point(23, 34, 'dark-light', 'orange', 'not-smooth')
    image.add_point(15, 35, 'dark-light', 'orange', 'not-smooth')
    image.draw()