"""
Bridge is a structural design pattern that lets you split a giant class or a set of closely
related classes into two separate hierarchies, abstraction and implementation,
which can be developed independently of each other.
"""
import logging

LOGGING_FORMAT = '%(asctime)s - %(message)s'
logging.basicConfig(level=logging.INFO, format=LOGGING_FORMAT)


class Device:
    def set_volume(self):
        logging.info('set_volume for %s', self.__class__.__name__)
        pass

    def set_channel(self):
        logging.info('set_channel for %s', self.__class__.__name__)
        pass

    def set_position(self):
        logging.info('set_position for %s', self.__class__.__name__)
        pass


class TV(Device):
    pass


class Radio(Device):
    pass


class Remote:
    def __init__(self, device):
        self.device = device

    def set_channel_remotely(self):
        logging.info('set_channel_remotely for %s', self.__class__.__name__)
        self.device.set_channel()

    def set_volume_remotely(self):
        logging.info('set_volume_remotely for %s', self.__class__.__name__)
        self.device.set_volume()


if __name__ == '__main__':
    tv = TV()
    r = Radio()
    #
    r.set_channel()
    tv.set_channel()
    #
    remote_radio = Remote(r)
    remote_tv = Remote(tv)
    #
    remote_radio.set_channel_remotely()
    remote_tv.set_volume_remotely()