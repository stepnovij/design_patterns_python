"""
Type:
The facade is a class that provides a simple interface to a complex subsystem containing dozens
of classes. The facade may have limited functionality in comparison to working with
subsystem directly. However, it includes only those features that clients really care about.

TakeAways:
1. Facade defines a new interface, whereas Adapter reuses an old interface.
2. Abstract Factory can be used as an alternative to Facade to hide platform-specific classes.
3. Facade can be transformed into Singleton since most of the time single facade object is sufficient.

Probable example:
json.loads
"""
import logging
import sys


LOGGING_FORMAT = '%(asctime)s - %(message)s'
logging.basicConfig(level=logging.INFO, format=LOGGING_FORMAT)


class VideoConverter:

    @staticmethod
    def convert(stream):
        logging.info('Starting video converter')
        return stream


class AudioConverter:

    @staticmethod
    def convert(stream):
        logging.info('Starting audio converter')
        return stream


class MP4Converter:

    def convert(self, stream):
        logging.info('Starting mp4 converter')
        stream = VideoConverter.convert(AudioConverter.convert(stream))
        return stream


class OGGConverter:

    def convert(self, stream):
        logging.info('Starting ogg converter')
        stream = VideoConverter.convert(AudioConverter.convert(stream))
        return stream


class Converter:
    def __init__(self):
        self._mp4 = MP4Converter()
        self._ogg = OGGConverter()

    def new_convert(self, to, file_):
        logging.info('Start facade class')
        res = None
        if to == 'mp4':
            res = self._mp4.convert(file_)
        elif to == 'ogg':
            res = self._ogg.convert(file_)
        else:
            raise Exception
        return res


if __name__ == '__main__':
    if len(sys.argv) > 2:
        convert_file = sys.argv[1]
        to = sys.argv[2]
        #
        facade = Converter()
        facade.new_convert(to, convert_file)
        #
    else:
        logging.info('Not enough args')