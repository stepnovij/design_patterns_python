"""
Decorator is a structural design pattern that lets you attach new behaviors to objects by placing
them inside wrapper objects that contain these behaviors.

TakeAways:
1. Adapter provides a different interface to its subject.
Proxy provides the same interface. Decorator provides an enhanced interface.
2. Decorator lets you change the skin of an object. Strategy lets you change the guts.
"""
import logging

LOGGING_FORMAT = '%(asctime)s - %(message)s'
logging.basicConfig(level=logging.INFO, format=LOGGING_FORMAT)


def decorator1(func):
    def wrapper(*args, **kwargs):
        logging.info('Before decorator')
        #
        res = func(*args, **kwargs)
        #
        logging.info('After decorator')
        return res
    return wrapper


@decorator1
def do_sm_math(a, b):
    logging.info('do_sm_math')
    return a + b


def decorator2(*dec_args, **dec_kwargs):
    def wrapper1(func):
        def wrapper(*args, **kwargs):
            print(' '.join(*dec_args))
            res = func(*args, **kwargs)
            return res
        return wrapper
    return wrapper1


@decorator2(['test', 'simple_data', 'with_decorator'])
def do_sm_math2(a, b):
    logging.info('do_sm_math')
    return a + b


def class_decorator(cls):
    def wrapper(*args, **kwargs):
        class B(cls):
            def __init__(self, *args, **kwargs):
                logging.info('init B')
                super().__init__(*args, **kwargs)
                self.some_other_data = 1
        return B(*args, **kwargs)
    return wrapper


@class_decorator
class A:
    def __init__(self, init_data):
        logging.info('init A')
        self.init_data = init_data


if __name__ == '__main__':
    print('####')
    res = do_sm_math(1, 2)
    print(res)
    #
    print('####')
    res = do_sm_math2(1, 2)
    print(res)
    print('####')
    a = A(init_data=2)
    print(type(a))
    print(a.init_data)
    print(a.some_other_data)