"""
TakeAways:
1. Adapter provides a different interface to its subject.
Proxy provides the same interface. Decorator provides an enhanced interface.
2. Adapter is meant to change the interface of an existing object.
Decorator enhances another object without changing its interface.
Decorator is thus more transparent to the application than an adapter is.
As a consequence, Decorator supports recursive composition, which isn't possible with pure Adapters.
"""
import logging
LOGGING_FORMAT = '%(asctime)s - %(message)s'
logging.basicConfig(level=logging.INFO, format=LOGGING_FORMAT)


class NoteBookPowerSocket1:

    def input1(self, power_socket1):
        logging.info(self.__class__.__name__)
        power_socket1.get_electricity1()


class LivingRoomPowerSocket2:

    def __init__(self):
        self.electricity = 'SOME electricity'

    def get_electricity2(self):
        logging.info(self.__class__.__name__)
        return self.electricity


class SteckerFrom2to1:

    def __init__(self, power_socket):
        self.power_socket = power_socket

    def get_electricity1(self):
        logging.info(self.__class__.__name__)
        self.power_socket.get_electricity2()


if __name__ == '__main__':
    socket2 = LivingRoomPowerSocket2()
    socket1 = SteckerFrom2to1(socket2)
    NoteBookPowerSocket1().input1(socket1)
