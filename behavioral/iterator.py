"""
Iterator = extract traversal method from the collection into separate object
"""


class SequenceGetItem:

    def __init__(self, data):
        self.data = data

    def __getitem__(self, item):
        return self.data[item]


class SequenceIterable:
    def __init__(self, data):
        self.data = data

    def __iter__(self):
        return SequenceIterator(self.data)


class SequenceIterator:
    def __init__(self, data):
        self.data = data
        self.position = 0

    def __next__(self):
        try:
            res = self.data[self.position]
            self.position += 1
            return res
        except IndexError:
            raise StopIteration

    def __iter__(self):
        return self


class SequenceYield:

    def __init__(self, data):
        self.data = data
        self.position = 0

    def __iter__(self):
        for x in self.data:
            yield x


if __name__ == '__main__':
    #
    print('##### getItem #####')
    l = [1, 2, 3, 5]
    seq1 = SequenceGetItem(l)
    for s in seq1:
        print(s)
    #
    print('##### Iterater #####')
    seq2 = SequenceIterable(l)
    for s in seq2:
        print(s)
    print('##### yield #####')
    seq3 = SequenceYield(l)
    for s in seq3:
        print(s)
    print('##### iteration with next #####')
    seq4 = iter(l)
    print(seq4)
    print(next(seq4))
    print(next(seq4))
    print(next(seq4))
    print(next(seq4))