"""
Dependency injection is a technique whereby one object supplies the dependencies of another object.
A dependency is an object that can be used (a service).
An injection is the passing of a dependency to a dependent object (a client) that would use it.
The service is made part of the client's state.
Passing the service to the client, rather than allowing a client to build or find the service,
is the fundamental requirement of the pattern.

https://www.martinfowler.com/articles/injection.html
"""