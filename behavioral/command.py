"""
Command helps to transform request into object

- parametrize operations in command. Delegation executing to other object and not
doing it by itself. Great analogy restaurant and order for cooking meal, waitress get all the needed
data and pasess it to chef, who transforms this menu to dish.

- also gives possiblity to queue commands and push their data or undo previous commands
- command history is a stack and each command:
  1. stores previous application state
  2. other solution is to have undo command

main classes:

Command
Receiver
Invoker
Client
"""

import logging
import random

LOGGING_FORMAT = '%(asctime)s - %(message)s'
logging.basicConfig(level=logging.INFO, format=LOGGING_FORMAT)


class BaseCommand:
    def __init__(self, receiver, app):
        self._receiver = receiver
        self.logging = logging
        self.app = app
        #
        self._start_state = None
        self._next_state = None

    def execute(self):
        raise NotImplemented

    def set_start_state(self):
        self._start_state = self.app.state

    def undo(self):
        self.app.state =self._start_state


class Receiver:
    def __init__(self, data):
        self._data = data

    def get_data(self):
        return self._data

    def set_new_data(self, data):
        self._data = data
        return self._data

    def delete_data(self):
        self._data = None


class ConcreteCommand1(BaseCommand):

    def execute(self):
        self.logging.info('Starting %s', self.__class__.__name__)
        self.set_start_state()
        some_data = self._receiver.delete_data()
        self.logging.info('%s get some kind of data %s', self.__class__.__name__, some_data)
        self.logging.info('Finishing %s', self.__class__.__name__)


class ConcreteCommand2(BaseCommand):

    def execute(self):
        self.logging.info('Starting %s', self.__class__.__name__)
        self.set_start_state()
        some_data = self._receiver.get_data()
        self.logging.info('%s get some kind of data %s', self.__class__.__name__, some_data)
        self.logging.info('Finishing %s', self.__class__.__name__)


class CommandHistory:
    def __init__(self):
        self.stack = []

    def push(self, command):
        logging.info("Pushing cmd to stack %s", command.__class__.__name__)
        self.stack.append(command)

    def pop(self):
        logging.info("Poping cmd from stack")
        return self.stack.pop()


class Client:
    def __init__(self, history):
        self.history = history
        self._state = random.randint(1, 10)

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, data):
        self._state = data

    def execute_command(self, cmd):
        if hasattr(cmd, 'execute'):
            cmd.execute()
            self.history.push(cmd)

    def undo_execute(self):
        cmd = self.history.pop()
        cmd.undo()


if __name__ == '__main__':
    cmd_history = CommandHistory()
    c = Client(cmd_history)
    r = Receiver('some specific data')
    #
    cmd1 = ConcreteCommand1(r, c)
    cmd2 = ConcreteCommand2(r, c)
    #
    c.execute_command(cmd1)
    c.execute_command(cmd2)
    #
    c.undo_execute()
    c.undo_execute()
